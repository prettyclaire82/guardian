## Guardian
Simple custom firewall used for the game GTA5

- [Requirements](#requirements)
  - [System](#system)
  - [Packages](#packages)
- [Build from source](#build-from-source)
- [Contributions](#contributions)
- [Changelog](CHANGELOG.md)
- [License](LICENSE)

## Requirements
#### System
- Python 3.6+ 64 bit
- Windows Vista/7/8/10 or Windows Server 2008 64 bit
- Administrator Privileges

#### Packages
##use pip install 

certifi==2019.6.16
chardet==3.0.4
colorama==0.4.1
cx-Freeze==5.1.1
idna==2.8
prompt-toolkit==2.0.9
pydivert==2.1.0
questionary==1.2.1
requests==2.22.0
six==1.12.0
tqdm==4.34.0
urllib3==1.25.3
wcwidth==0.1.7


## Build from source
Run `make_exe.cmd` or
```
set TCL_LIBRARY=P:\Program Files (x86)\Python36\tcl\tcl8.6
set TK_LIBRARY=P:\Program Files (x86)\Python36\tcl\tk8.6
python setup.py build
```

## Contributions
All contributions are helpful, feel free to make a Merge Request.